package org.launchcode.training;


import org.junit.Test;

import static junit.framework.TestCase.fail;
import static org.junit.Assert.assertEquals;

public class CarTest {

    @Test
    public void emptyTest() {
        assertEquals(10, 10, .001);
    }

    @Test
    public void testGasTankLevelOnInit() {
        Car testCar = new Car("Ford", "Mustang", 10, 20);
        assertEquals(10, testCar.getGasTankLevel(), .001);
    }

    @Test
    public void testGasTankLevelAfterDriving() {
        Car testCar = new Car("Ford", "Mustang", 10, 20);
        testCar.drive(20);
        assertEquals(9, testCar.getGasTankLevel(), .001);
    }

    @Test
    public void testGasTankLevelAfterExceedingRange() {
        Car testCar = new Car("Ford", "Mustang", 10, 20);
        testCar.drive(250);
        assertEquals(0, testCar.getGasTankLevel(), .001);
    }

    //TODO: can't have more gas than tank size, expect an exception
    @Test(expected = IllegalArgumentException.class)
    public void testGasTankLevelExceedsTankSize() {
        Car testCar = new Car("Ford", "Mustang", 10, 20);
        testCar.addGas(5);
        fail("Exception not thrown on addGas()");
    }

}
